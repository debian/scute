scute (1:1.7.0+git20240826+8331f37-1) unstable; urgency=medium

  [ NIIBE Yutaka ]
  * New from upstream.
  * debian/scute.install: Update.
  * debian/rules: Add --enable-maintainer-mode for version.texi
    generation.

  [ Helmut Grohne ]
  * Fix FTCBFS: Pass --with-gpgsm and --with-gpg-agent to configure.
    (Closes: #905380)

  [ Debian Janitor]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on libassuan-dev and
      libgpg-error-dev.

 -- NIIBE Yutaka <gniibe@fsij.org>  Wed, 18 Dec 2024 15:36:22 +0900

scute (1:1.7.0+git20230405+a5d8355-1) experimental; urgency=low

  * New from upstream.
  * debian/control: Update.  Adding texinfo and gpgrt-tools.
  * debian/copyright: Update to follow upstream licence change.
  * debian/compat: Remove.
  * debian/scute.info: Remove, following upstream change.
  * debian/patches/scute.git-8b01c156.patch: Remove.
  * debian/patches/scute.git-f31e6af6.patch: Remove.

 -- NIIBE Yutaka <gniibe@fsij.org>  Thu, 20 Apr 2023 10:11:03 +0900

scute (1:1.5.0-1) unstable; urgency=medium

  * New from upstram: Final released version of 1.5.0.

    - Since upstream Git version said it's 1.5.0-something and released
      version is 1.5.0, we introduced epoch from this Debian version.

  * debian/patches/00-avoid-version-name-without-lib.diff: Remove.
  * debian/patches/01-agent-gnupg21.diff: Remove.
  * debian/patches/02-add-error-handling.diff: Remove.
  * debian/patches/scute.git-8b01c156.patch: New.
  * debian/patches/scute.git-f31e6af6.patch: New.
  * debian/compat: Upgrade to 11.
  * debian/control (Standards-Version): Conforms to 4.2.1.
  (Build-Depends): Requiring debhelper >=11, remove autotools-dev
  and automake.
  (Priority): Change to optional (was: extra).
  * debian/rules (%): Remove --with autoreconf, as it's default now.
  (override_dh_makeshlibs): Invoke dh_makeshlibs with -n.
  * debian/docs: Update.
  * debian/scute.info: Update.
  * Confirmed that make check works again (Closes: #878470).

 -- NIIBE Yutaka <gniibe@fsij.org>  Wed, 16 Jan 2019 13:34:52 +0900

scute (1.5.0+git20160808+94eeb2d-2) unstable; urgency=medium

  * To accommodate the change of possible error codes of scdaemon
    in GnuPG 2.1, add debian/patches/02-add-error-handling.diff from 
    upstream(Closes: #852895).  Thanks to Lucas Nussbaum.

 -- NIIBE Yutaka <gniibe@fsij.org>  Wed, 01 Feb 2017 09:54:00 +0900

scute (1.5.0+git20160808+94eeb2d-1) unstable; urgency=medium

  * New from upstram Git repo.
  * debian/copyright: Update.
  * README.Debian: Update.
  * debian/control (Standards-Version): Conforms to 3.9.8.
  * debian/patches/01-agent-gnupg21.diff: Refresh.

 -- NIIBE Yutaka <gniibe@fsij.org>  Tue, 01 Nov 2016 15:13:48 +0900

scute (1.5.0+git20151221.dc22111-2) unstable; urgency=low

  * Confirmed that documentation was updated (Closes: #790891).
  * Confirmed that the problem was fixed in GNOME (Closes: #745055).
  * debian/control (Build-Depends): Add scdaemon.

 -- NIIBE Yutaka <gniibe@fsij.org>  Mon, 21 Dec 2015 06:51:30 +0000

scute (1.5.0+git20151221.dc22111-1) unstable; urgency=low

  * New from upstream Git repo.
  * debian/patches/01-agent-gnupg21.diff: New.  Thanks to Chris Lamb.
    Closes: #802302.
  * debian/control (Standards-Version): Conforms to 3.9.6.
  * debian/patches/00-avoid-version-name-without-lib.diff: Remove.

 -- NIIBE Yutaka <gniibe@fsij.org>  Mon, 21 Dec 2015 03:03:14 +0000

scute (1.4.0-4) unstable; urgency=low

  * debian/rules (override_dh_auto_clean, override_dh_auto_configure): Remove.
    (override_dh_auto_test): Invoke dh_auto_test under gpg-agent.
    - Set debian/dot-gnupg/scdaemon.conf with debug-disable-ticker to work
      around a bug of GnuPG.
    - Invoke rm -rf debian/dot-gnupg.

 -- NIIBE Yutaka <gniibe@fsij.org>  Wed, 04 Apr 2012 09:19:34 +0900

scute (1.4.0-3) unstable; urgency=low

  * debian/compat: Require 9.
  * debian/copyright (Format): Fix.
  * debian/control, debian/rules, debian/scute.install: Add multiarch support.
  * debian/README.Debian: Mention multiarch.

 -- NIIBE Yutaka <gniibe@fsij.org>  Mon, 02 Apr 2012 16:28:08 +0900

scute (1.4.0-2) unstable; urgency=low

  [ Mònica Ramírez Arceda <monica@probeta.net> ]
  * debian/control (Build-Depends): Use dh-autoreconf to build the package
    (Closes: #662941).
  * debian/control (Build-Depends): Remove quilt dependency.
  * debian/copyright: Fix errors to fulfill standard machine-readable
    format.

  [ NIIBE Yutaka ]
  * debian/control (Standards-Version): Updated to 3.9.2 as we fixed
    debian/copyright.

 -- NIIBE Yutaka <gniibe@fsij.org>  Thu, 29 Mar 2012 13:19:42 +0900

scute (1.4.0-1) unstable; urgency=low

  * Initial release (Closes: #615930).

 -- NIIBE Yutaka <gniibe@fsij.org>  Fri, 04 Mar 2011 14:54:12 +0900
