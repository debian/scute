Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: scute
Upstream-Contact: Damien Goutte-Gattat <dgouttegattat@incenp.org>
Source: https://gnupg.org/software/scute/index.html
Copyright: (C) 2006, 2007, 2008, 2009 g10 Code GmbH

Files: *
Copyright: (C) 2006, 2007, 2008, 2009, 2015, 2019 g10 Code GmbH
License: LGPL-2.1-or-later
 Scute is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of
 the License, or (at your option) any later version.
 .
 Scute is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program; if not, see <https://gnu.org/licenses/>.
 SPDX-License-Identifier: LGPL-2.1-or-later
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 /usr/share/common-licenses/LGPL-2.1.

Files: src/pkcs11.h
Copyright: 2006, 2007 g10 Code GmbH
           2006 Andreas Jellinghaus
License: unlimited
 This file is free software; as a special exception the author gives
 unlimited permission to copy and/or distribute it, with or without
 modifications, as long as this notice is preserved.

Files: doc/scute.texi
Copyright: (C) 2006, 2007, 2008, 2009, 2010, 2017, 2019, 2020 g10 Code GmbH.
License: LGPL-2.1-or-later
 The Scute Manual is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.  The text of the
 license can be found in the section entitled ``Library Copying''.
 .
 The Scute Manual is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 /usr/share/common-licenses/LGPL-2.1.

Files: debian/*
Copyright: 2011 NIIBE Yutaka <gniibe@fsij.org>
License: CC0-1.0
 Copying and distribution of this package, with or without
 modification, are permitted in any medium without royalty,
 under CC0 1.0.
